export const formatDate = (date: Date) => {
  return date.toLocaleDateString().replace(/\//g, ".");
};

export const isDateInPast7Days = (dateToCheck: Date): boolean => {
  const sevenDaysAgo = new Date();
  sevenDaysAgo.setDate(sevenDaysAgo.getDate() - 7);

  return dateToCheck >= sevenDaysAgo && dateToCheck <= new Date();
};

export const categoryStringtoArrayOfCategories = (
  category: string
): string[] => {
  let categories: string[] = [];
  if (category) {
    const parts = category
      .replace(/en:\s+/g, "")
      .split(",");

    const validParts = parts.filter(
      (part) => part.trim() !== ""
    );

    const cleanedParts = validParts.map((part) => part.trim());

    categories = [...cleanedParts];
  }

  return categories;
};
