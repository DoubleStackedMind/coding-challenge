import { MaterialCommunityIcons } from "@expo/vector-icons";
import React, { useEffect, useState } from "react";
import {
  ActivityIndicator,
  RefreshControl,
  StyleSheet,
  View
} from "react-native";
import { Appbar, FAB } from "react-native-paper";
import { useSelector, useDispatch } from "react-redux";
import { selectors, actions, Inventory } from "./store/inventory";
import { RootState } from "./store";
import { SafeAreaView } from "react-native-safe-area-context";
import { StackScreenProps } from "@react-navigation/stack";
import { StackParamList } from "./App";
import { ProductItem } from "./ProductItem";
import { FlatList } from "react-native-gesture-handler";

export default (props: StackScreenProps<StackParamList, "Home">) => {
  const fetching = useSelector((state: RootState) => state.inventory.fetching);
  const inventory = useSelector(selectors.selectInventory);
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState<Inventory[]>([]);

  const MemoizedProductItem = React.memo(ProductItem);

  useEffect(() => {
    const unsubscribe = props.navigation.addListener("focus", () => {
      dispatch(actions.fetchInventory());
    });
    return unsubscribe;
  }, [props.navigation]);

  useEffect(() => {
    setData(inventory.slice(0, 10));
  }, [inventory]);

  const onRefresh = () => {
    dispatch(actions.fetchInventory());
  };

  function navigateToCamera() {
    props.navigation.navigate("Camera");
  }

  const renderItem = ({ item }: { item: Inventory }) => (
    <View style={styles.cardContainer}>
      <MemoizedProductItem
        imageUrl={item.fields["Product Image"]}
        title={item.fields["Product Name"]}
        date={new Date(item.fields.Posted)}
        category={item.fields["Product Categories"]}
      />
    </View>
  );

  function handleOffset(arg0: Inventory[], offset: number) {
    if (!loading) {
      setLoading(true);
      setData((prevData) => [...prevData, ...arg0.slice(data.length, data.length+offset)]);
      setLoading(false);
    }
  }

  return (
    <View style={{ flex: 1, backgroundColor: "white" }}>
      <Appbar.Header>
        <Appbar.Content titleStyle={styles.appBarTitle} title="Inventory" />
      </Appbar.Header>

      <FlatList
        data={data}
        renderItem={renderItem}
        keyExtractor={(item, index) => index.toString()}
        refreshControl={
          <RefreshControl refreshing={fetching} onRefresh={onRefresh} />
        }
        onEndReached={() => {
          handleOffset(inventory, 10);
        }}
        onEndReachedThreshold={0.1}
        ListFooterComponent={() =>
          loading ? <ActivityIndicator size="large" color="blue" /> : null
        }
      />

      <SafeAreaView style={styles.fab}>
        <FAB
          icon={() => (
            <MaterialCommunityIcons name="barcode" size={24} color="#0B5549" />
          )}
          label="Scan Product"
          onPress={navigateToCamera}
        />
      </SafeAreaView>
    </View>
  );
};

const styles = StyleSheet.create({
  fab: {
    position: "absolute",
    bottom: 16,
    width: "100%",
    flex: 1,
    alignItems: "center"
  },
  cardContainer: {
    marginBottom: 12
  },
  appBarTitle: {
    fontSize: 24,
    fontStyle: "normal",
    fontWeight: "400",
    lineHeight: 22,
    justifyContent: "center",
    marginLeft: "auto",
    marginRight: "auto"
  }
});
