import React, { useState } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { ImageContainer } from "./components/ImageContainer";
import { NewIcon } from "./components/icons/NewIcon";
import { ChevronUp } from "./components/icons/ChevronUp";
import { TagContainer } from "./components/TagContainer";
import { PlaceholderIcon } from "./components/icons/placeholder";
import {
  categoryStringtoArrayOfCategories,
  formatDate,
  isDateInPast7Days
} from "./utils/utils";
import { ProductStyles } from "./styles/styles";


export type ProductItemProps = {
  title: string;
  imageUrl?: string;
  date: Date;
  category?: string;
};

export const ProductItem: React.FC<ProductItemProps> = ({
  title,
  imageUrl = "",
  date,
  category = ""
}) => {
  const [isExpanded, setIsExpanded] = useState(true);

  function toggleCard() {
    setIsExpanded(!isExpanded);
  }

  const categoriesArray = categoryStringtoArrayOfCategories(category);

  return (
    <View style={ProductStyles.card}>
      <View style={ProductStyles.container}>
        <View>
          {imageUrl ? (
            <ImageContainer
              style={[
                isExpanded
                  ? ProductStyles.ExpandedImage
                  : ProductStyles.CollapsedImage
              ]}
              imageUrl={imageUrl}
            />
          ) : (
            <PlaceholderIcon
              style={[
                isExpanded
                  ? ProductStyles.ExpandedImage
                  : ProductStyles.CollapsedImage
              ]}
            />
          )}
        </View>

        <View style={ProductStyles.detailsContainer}>
          <TouchableOpacity onPress={toggleCard}>
            <View style={ProductStyles.topContainer}>
              <View style={ProductStyles.header}>
                <Text textBreakStrategy="balanced" style={ProductStyles.title}>
                  {title ?? "Name was not provided"}
                </Text>
                <View style={ProductStyles.rightConainer}>
                  {isDateInPast7Days(date) && <NewIcon />}

                  <ChevronUp
                    style={[
                      !isExpanded && {
                        transform: [{ rotate: "180deg" }]
                      }
                    ]}
                  />
                </View>
              </View>

              <Text style={{ paddingTop: 6 }}>{formatDate(date)}</Text>
            </View>
          </TouchableOpacity>
          {isExpanded && (
            <View style={ProductStyles.categoriesContainer}>
              {categoriesArray &&
                categoriesArray.map((ctg, index) => (
                  <TagContainer key={index} label={ctg}></TagContainer>
                ))}
            </View>
          )}
        </View>
      </View>
    </View>
  );
};
