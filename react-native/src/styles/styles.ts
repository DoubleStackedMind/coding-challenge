import { StyleSheet, Platform, Dimensions } from "react-native";

export const ProductStyles = StyleSheet.create({
  card: {
    display: "flex",
    flexDirection: "row",
    padding: 8,
    alignItems: "flex-start",
    alignSelf: "center",
    gap: 12,
    width: Dimensions.get("window").width * 0.8,
    backgroundColor: "#F8F9FC",
    borderRadius: 4,
    ...Platform.select({
      ios: {
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        shadowColor: "rgba(0, 0, 0, 0.25)"
      },
      android: {
        elevation: 4
      }
    })
  },
  container: {
    flex: 1,
    display: "flex",
    flexDirection: "row",
    gap: 12
  },
  detailsContainer: {
    display: "flex",
    flexDirection: "column",
    gap: 12,
    flex: 1
  },
  topContainer: {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start"
  },
  header: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    gap: 12,
    flex: 1
  },
  rightConainer: {
    display: "flex",
    flexDirection: "row",
    flex: 1,
    gap: 12,
    justifyContent: "flex-end"
  },
  title: {
    fontFamily: "Roboto",
    fontSize: 20,
    fontStyle: "normal",
    fontWeight: "900",
    lineHeight: 22,
    width: "50%"
  },
  categoriesContainer: {
    display: "flex",
    flexDirection: "row",
    flex: 1,
    gap: 6,
    flexWrap: "wrap",
    alignContent: "flex-end",
    marginBottom: 4
  },
  ExpandedImage: {
    width: 85,
    height: 133,
    flexShrink: 0
  },
  CollapsedImage: {
    width: 85,
    height: 64,
    flexShrink: 0,
    resizeMode: "contain"
  }
});
