import * as React from "react";
import Svg, { Path, G } from "react-native-svg";
import { StyleSheet, View } from "react-native";

export const NewIcon = () => {
  return (
    <View style={styles.iconContainer}>
      <Svg width="29" height="10" viewBox="0 0 29 10" fill="black">
        <G id="NEW">
          <Path
            d="M7.92031 10H6.18539L1.73493 2.90522V10H0V0H1.73493L6.1991 7.12225V0H7.92031V10Z"
            fill="white"
          />
          <Path
            d="M15.8063 5.53571H11.7056V8.61264H16.4989V10H9.97068V0H16.4509V1.4011H11.7056V4.16209H15.8063V5.53571Z"
            fill="white"
          />
          <Path
            d="M25.7427 7.52747L27.2788 0H29L26.6959 10H25.0364L23.1369 2.69918L21.1963 10H19.5299L17.2258 0H18.947L20.4968 7.51374L22.4032 0H23.8569L25.7427 7.52747Z"
            fill="white"
          />
        </G>
      </Svg>
    </View>
  );
};

const styles = StyleSheet.create({
    iconContainer: {
      display: 'flex',
      flex: 1,
      height: 26,
      width: 53,
      paddingHorizontal: 12,
      paddingVertical: 8,
      alignItems: 'center',
      backgroundColor: '#333333',
      borderTopLeftRadius: 9,
      borderBottomLeftRadius: 9,
      borderBottomRightRadius: 9,
    }
  });
  
