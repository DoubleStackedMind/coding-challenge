import React from "react";
import { StyleSheet, View, Text } from "react-native";

export type TagProps = {
  label?: string;
};

export const TagContainer: React.FC<TagProps> = ({ label }) => {
  return (
    <View style={styles.defaulTag}>
      <Text numberOfLines={1} ellipsizeMode="tail" style={styles.label}>
        {label}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  defaulTag: {
    borderRadius: 48,
    paddingVertical: 2,
    paddingHorizontal: 12,
    display: "flex",
    backgroundColor: "#D4E5FF",
    height: 26,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center"
  },

  label: {
    fontSize: 12,
    color: "#1B2633",
    fontWeight: "400",
    lineHeight: 22,
    fontFamily: "Roboto"
  }
});
