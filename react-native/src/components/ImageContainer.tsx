import React from "react";
import { Image, ImageStyle, StyleProp } from "react-native";

export type ImageProps = {
  imageUrl: string;
  style?: StyleProp<ImageStyle>;
};

export const ImageContainer: React.FC<ImageProps> = ({ imageUrl, style }) => {
  return <Image source={{ uri: imageUrl }} style={style} />;
};
